機能仕様書

SQL作成コード

create DATABASE user default character set utf8;

use user;

create table user
(id serial ,
login_id varchar(255) unique not null,
name varchar(255) not null,
birth_date date not null,
password varchar(255) not null,
create_date datetime not null,
update_date datetime);

insert into user 
values(1,'admin','管理者','1977-12-23','password',cast(now() as datetime),cast(now() as datetime));

alter table user modify column update_date not null;

insert into user 
values(2,'tanaka','田中二郎','1986-6-25','pass',cast(now() as datetime),cast(now() as datetime));

insert into user 
values(3,'yamada','ヤマダ電機','1966-8-30','pass',cast(now() as datetime),cast(now() as datetime));

insert into user 
values(4,'mitubishi','三菱商事','1950-7-14','pass',cast(now() as datetime),cast(now() as datetime));

insert into user(login_id,name,birth_date,password,create_date,update_date)
values('aiku','アイク','19190223','pass',cast(now() as datetime),cast(now() as datetime));

UPDATE user SET password=1A1DC91C907325C69271DDF0C944BC72 WHERE id=2;