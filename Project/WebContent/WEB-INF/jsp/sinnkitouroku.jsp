<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				<p>${sessionScope.userInfo.name}さん
					<a href="userLogout" class="alert-link">ログアウト</a>
				</p>

			</div>
		</header>
	</div>
<form action="UserSinki" method="post">
<div align="center">
<h1>ユーザー新規登録</h1>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>

<p>ログインID                 <input type="text" name="sinkiLoId"></p>
<p>パスワード                 <input type="password" name="sinkiPass"></p>
<p>パスワード(確認)        <input type="password" name="sinkiCoPass"></p>
<p>ユーザー名                 <input type="text" name="sinkiUsNm"></p>
<p>生年月日                    <input type="text" name="sinkiUsBirth">​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
			<p><input type="submit" value="登録"></p>
		</div>
</form>
<a href="UserItiran" class="alert-link">戻る</a>

</body>
</html>