<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
    <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				<p>${sessionScope.userInfo.name}さん
					<a href="userLogout" class="alert-link">ログアウト</a>
				</p>

			</div>
		</header>
	</div>
	<div align="center">
<form action="UserJyouhouKousin" method="post">

<h1>ユーザー情報更新</h1>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>

	<input type=hidden name="upId" value="${user.id}">
<table>
<tr>
					<th></th>
					<th></th>
				</tr>
<tr>
<td> ログインID</td>
<td>${user.loginId}</td>
</tr>
<tr>
<td>パスワード  </td>
<td>      <input type="password" name="upPass"></td>
</tr>
<tr>
<td>パスワード(確認)</td>
 <td>       <input type="password" name="upConfPass"></td>
 </tr>
<tr>
<td>ユーザー名 </td>
<td>     <input type="text" name="upUserNm" value="${user.name}"></td>
</tr>
<tr>
<td>生年月日    </td>
<td>    <input type="text" name="upUserBirth" value="${user.birthDate}"></td>
</tr>
</table>
<input type="submit" value="更新">


</form>
</div>
<a href="UserItiran" class="alert-link">戻る</a>
</body>
</html>