<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー 一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				<p>${sessionScope.userInfo.name}さん
					<a href="userLogout" class="alert-link">ログアウト</a>
				</p>

			</div>
		</header>
	</div>
	<form action="UserItiran" method="post">
		<div align="center">
			<h1>ユーザー 一覧</h1>

			<div align="right">
			<a href="UserSinki" class="alert-link">新規登録</a>
			</div>
			<p>
				ログインID <input type="text" name="itiranId">
			</p>
			<p>
				ユーザー名 <input type="text" name="itiranUserNm">
			</p>
			<p>
				生年月日 <input type="text" name="itiranUserBirthA"
					placeholder="年 / 月 / 日">～<input type="text"
					name="itiranUserBirthB" placeholder="年 / 月 / 日">
			</p>
		</div>
		<div align="right">
			<input type="submit" value="検索">
		</div>
		</form>

		<div align="center">
		<c:choose>
		<c:when test="${sessionScope.userInfo.loginId == 'admin'}">
			<table  border="3">
				<tr>
					<th>ログインID</th>
					<th>ユーザー名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
				<c:forEach var="member" items="${userListb}">
					<tr>
						<td>${member.loginId}</td>
						<td>${member.name}</td>
						<td>${member.birthDate}</td>
												<td>
						<a class="btn btn-primary" href="UserSyousaiSannsyou?id=${member.id}">
								詳細
								</a>
								<a class="btn btn-success" href="UserJyouhouKousin?id=${member.id}" >
								更新
								</a>
								<a href="UserSakujyoKakunin?id=${member.id}" class="btn btn-danger">
								削除
								</a>
								</td>
					</tr>
				</c:forEach>
			</table>
			</c:when>
			<c:otherwise>
						<table  border="3">
				<tr>
					<th>ログインID</th>
					<th>ユーザー名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
				<c:forEach var="member" items="${userListb}">
					<tr>
						<td>${member.loginId}</td>
						<td>${member.name}</td>
						<td>${member.birthDate}</td>

								<td>
								<a class="btn btn-primary" href="UserSyousaiSannsyou?id=${member.id}">
								詳細
								</a>
								<c:if test="${sessionScope.userInfo.loginId== member.loginId}">
								<a class="btn btn-success" href="UserJyouhouKousin?id=${member.id}" >
								更新
								</a>
								</c:if>
						</td>
					</tr>
				</c:forEach>
			</table>
			</c:otherwise>
			</c:choose>
			</div>
</body>
</html>