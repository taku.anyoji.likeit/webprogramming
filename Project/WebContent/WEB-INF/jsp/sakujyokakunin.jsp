<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
    <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				<p>${sessionScope.userInfo.name}さん
					<a href="userLogout" class="alert-link">ログアウト</a>
				</p>

			</div>
		</header>
	</div>
		<div align="center">

<form action="UserSakujyoKakunin" method="post">

<p> ログインID：${user.loginId}</p>

<p>を本当に削除してもよろしいでしょうか。</p>

  <button name="canDele" value="${user.id}">戻る</button>       <button name="delete" value="${user.id}">OK</button>

</form>

</div>

</body>
</html>