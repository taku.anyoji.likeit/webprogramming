<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
    <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				<p>${sessionScope.userInfo.name}さん
					<a href="userLogout" class="alert-link">ログアウト</a>
				</p>

			</div>
		</header>
	</div>
	<div align="center">
<form action="UserSyousaiSannsyou" method="post">

<h1>ユーザー情報詳細参照</h1>
		<table>
				<tr>
					<th></th>
					<th></th>
				</tr>
<tr>
<td> ログインID </td>
<td>${user.loginId}</td>
  </tr>
<tr>
<td>ユーザー名</td>
<td>  ${user.name}</td>
</tr>
<tr>
<td>生年月日 </td>
<td>    ${user.birthDate}</td>
</tr>
<tr>
<td> 登録日時  </td>
<td>   ${user.createDate}</td>
</tr>
<tr> <td>更新日時  </td>
<td>   ${user.updateDate}</td>
</tr>
</table>
</form>
<a href="UserItiran" class="alert-link">戻る</a>
</div>
</body>
</html>