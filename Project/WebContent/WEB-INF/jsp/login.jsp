<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>

<div  align="center">
<h1>ログイン画面</h1>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
<c:if test="${userInfo!=null} ">
	<c:redirect url="UserItiran">
	<c:param name="loginId" value="loginId"></c:param>
	<c:param name="pass" value="pass"></c:param>
	</c:redirect>
</c:if>
<form action="UserServlet" method="post">

<p> ログインID        <input type="text" name="loginId"></p>
 <p>パスワード        <input type="password" name="pass"></p>


<input type="submit" value="ログイン">

</form>
</div>

</body>
</html>