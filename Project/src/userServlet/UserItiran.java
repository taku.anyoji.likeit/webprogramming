package userServlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserItiran
 */
@WebServlet("/UserItiran")
public class UserItiran extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserItiran() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("UserServlet");
		}else{
		UserDao userDaob=new UserDao();
		List<User> userListb=userDaob.findAllb();

		request.setAttribute("userListb", userListb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itiran.jsp");
		dispatcher.forward(request, response);

	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String itiranId=request.getParameter("itiranId");
		String itiranUserNm=request.getParameter("itiranUserNm");
		String itiranUserBirthA=request.getParameter("itiranUserBirthA");
		String itiranUserBirthB=request.getParameter("itiranUserBirthB");

		UserDao userDao = new UserDao();
		List<User> userListA=userDao.findCase(itiranId,itiranUserNm,itiranUserBirthA,itiranUserBirthB);

		request.setAttribute("userListb", userListA);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itiran.jsp");
		dispatcher.forward(request, response);

	}
}
