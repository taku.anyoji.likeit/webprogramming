package userServlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserJyouhouKousin
 */
@WebServlet("/UserJyouhouKousin")
public class UserJyouhouKousin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserJyouhouKousin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("UserServlet");
		}else {
		String id = request.getParameter("id");

		UserDao userDao=new UserDao();
		User user=userDao.findAll(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/jyouhoukousin.jsp");
		dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String upId=request.getParameter("upId");
		String upPass=request.getParameter("upPass");
		String upConfPass=request.getParameter("upConfPass");
		String upUserNm=request.getParameter("upUserNm");
		String upUserBirth=request.getParameter("upUserBirth");

		UserDao userDao=new UserDao();
		User user=new User();
		//登録失敗パターン
		if(upUserNm.length()<1||upUserBirth.length()<1) {
			request.setAttribute("error", "入力された内容は正しくありません");
			user=userDao.findAll(upId);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/jyouhoukousin.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(!(upPass.equals(upConfPass))) {
			request.setAttribute("error", "入力された内容は正しくありません");
			user=userDao.findAll(upId);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/jyouhoukousin.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//登録成功パターン
		    //パスワード未入力
		if(upPass.length()<1) {
			int result=userDao.update(upId, upUserNm, upUserBirth);
			if(result==1) {
				response.sendRedirect("UserItiran");
			}else {
				request.setAttribute("error", "入力された内容は正しくありません");
				user=userDao.findAll(upId);
				request.setAttribute("user", user);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/jyouhoukousin.jsp");
				dispatcher.forward(request, response);
			}
		}else {
			System.out.println(upId);
		String MDPass=userDao.pass(upPass);
		int result=userDao.update(upId,MDPass, upUserNm, upUserBirth);
		if(result==1) {
			response.sendRedirect("UserItiran");
		}else {
			request.setAttribute("error", "入力された内容は正しくありません");
			user=userDao.findAll(upId);
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/jyouhoukousin.jsp");
			dispatcher.forward(request, response);
		}
	}
	}


	}


