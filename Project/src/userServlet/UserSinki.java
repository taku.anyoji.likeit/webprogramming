package userServlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSinki
 */
@WebServlet("/UserSinki")
public class UserSinki extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSinki() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("UserServlet");
		}else {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinnkitouroku.jsp");
		dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String sinkiLoId=request.getParameter("sinkiLoId");
		String sinkiPass=request.getParameter("sinkiPass");
		String sinkiCoPass=request.getParameter("sinkiCoPass");
		String sinkiUsNm=request.getParameter("sinkiUsNm");
		String sinkiUsBirth=request.getParameter("sinkiUsBirth");

		UserDao userDao=new UserDao();

		//登録失敗パターン
			if(sinkiLoId.length()<1||sinkiUsNm.length()<1||sinkiUsBirth.length()<1) {
				request.setAttribute("error", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinnkitouroku.jsp");
				dispatcher.forward(request, response);
				return;
			}
		if(!(sinkiPass.equals( sinkiCoPass))) {
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinnkitouroku.jsp");
			dispatcher.forward(request, response);
			return;
		}


		//登録成功パターン
		String MDPass=userDao.pass(sinkiPass);
		int result=userDao.insert(sinkiLoId, MDPass, sinkiUsNm, sinkiUsBirth);
		if(result==1) {
			response.sendRedirect("UserItiran");
		}else {
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinnkitouroku.jsp");
			dispatcher.forward(request, response);
		}
	}
}
