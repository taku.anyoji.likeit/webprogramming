package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	     //暗号化メソッド
	public String pass(String password){
		//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = null;
				try {
					bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				String result = DatatypeConverter.printHexBinary(bytes);

				return result;
	}

	//ログインメソッド
	public User findByLoginInfo(String loginId, String MDPass) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//  ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id =? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, MDPass);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (false == rs.next()) {
				return null;
			}
			// ログイン成功時の処理		・・・②
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			return new User(loginIdDate, nameDate, birthDate);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規登録メソッド
	public int insert(String sinkiLoId,String MDPass,String sinkiUsNm,String sinkiUsBirth) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) "
					+ "VALUES(?,?,?,?,cast(now() as datetime),cast(now() as datetime));";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, sinkiLoId);
			pStmt.setString(2, sinkiUsNm);
			pStmt.setString(3, sinkiUsBirth);
			pStmt.setString(4, MDPass);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//情報更新メソッド
	     //全入力パターン
	public int update(String upId,String MDPass,String upUserNm,String upUserBirth) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="UPDATE user SET name=?,birth_date=?,password=?,update_date=cast(now() as datetime) WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, upUserNm);
			pStmt.setString(2, upUserBirth);
			pStmt.setString(3, MDPass);
			pStmt.setString(4, upId);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

		//password未入力パターン
	public int update(String upId,String upUserNm,String upUserBirth) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="UPDATE user SET name=?,birth_date=?,update_date=cast(now() as datetime) WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, upUserNm);
			pStmt.setString(2, upUserBirth);
			pStmt.setString(3, upId);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

//ユーザー削除機能
	public int delete(String deId) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="DELETE FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, deId);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

//ユーザー一覧表示メソッド
	public List<User> findAllb() {
		Connection con = null;
		List<User> userListb = new ArrayList<User>();
		Statement stmt = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT id,login_id,name,birth_date FROM user WHERE NOT id=1 ";

			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id=rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");

				User user = new User(id,loginId, name, birthDate);
				userListb.add(user);
			}
			return userListb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー条件検索メソッド

	public List<User> findCase(String itiranId,String itiranUserNm,String itiranUserBirthA,String itiranUserBirthB) {
		Connection con = null;
		List<User> userListA = new ArrayList<User>();
		Statement stmt = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE NOT id=1";
					if(itiranId.length()>0) {
						sql=sql+" AND login_id='"+itiranId+"'";
					}
					if(itiranUserNm.length()>0) {
						sql=sql+" AND name LIKE '%"+itiranUserNm+"%' ";
					}
					if(itiranUserBirthA.length()>0 && itiranUserBirthB.length()>0) {
						sql=sql+" AND birth_date BETWEEN '"+itiranUserBirthA+"' and '"+itiranUserBirthB+"'";
					}else if(itiranUserBirthA.length()>0 && itiranUserBirthB.length()<1){
						sql=sql+" AND birth_date>='"+itiranUserBirthA+"'";
					}else if(itiranUserBirthA.length()<1 && itiranUserBirthB.length()>0) {
						sql=sql+" AND birth_date<='"+itiranUserBirthB+"'";
					}

					stmt = con.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					while (rs.next()) {
						int id=rs.getInt("id");
						String loginId = rs.getString("login_id");
						String name = rs.getString("name");
						String birthDate = rs.getString("birth_date");

						User user = new User(id,loginId, name, birthDate);
						userListA.add(user);
					}
					return userListA;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public User findAll(String id) {
		Connection con = null;
		PreparedStatement pStmt=null;
		User user=new User();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT id,login_id,name,birth_date,create_date,update_date FROM user Where id=? ";

			pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
			int idDate=rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");


			user= new User(idDate,loginIdDate, nameDate, birthDate,createDate,updateDate);
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (pStmt != null) {
				try {
					pStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
